package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.enumerated.Status;
import ru.t1.gorodtsova.tm.enumerated.TaskSort;
import ru.t1.gorodtsova.tm.dto.model.TaskDTO;

import java.util.Collection;
import java.util.List;

public interface ITaskService {

    @NotNull
    TaskDTO add(@Nullable TaskDTO task);

    @NotNull
    TaskDTO add(@Nullable String userId, @Nullable TaskDTO task);

    @NotNull
    Collection<TaskDTO> add(@Nullable Collection<TaskDTO> tasks);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name);

    @NotNull
    TaskDTO create(@Nullable String userId, @Nullable String name, @Nullable String description);

    @NotNull
    Collection<TaskDTO> set(@Nullable Collection<TaskDTO> tasks);

    @Nullable
    List<TaskDTO> findAll();

    @Nullable
    List<TaskDTO> findAll(@NotNull String userId);

    @Nullable
    List<TaskDTO> findAll(@Nullable String userId, @Nullable TaskSort sort);

    @NotNull
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO findOneById(@Nullable String userId, @Nullable String id);

    void removeAll();

    void removeAll(@Nullable String userId);

    @NotNull
    TaskDTO removeOneById(@Nullable String userId, @Nullable String id);

    @NotNull
    TaskDTO updateById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable String name,
            @Nullable String description
    );

    @NotNull
    TaskDTO changeTaskStatusById(
            @Nullable String userId,
            @Nullable String id,
            @Nullable Status status
    );

    int getSize();

    int getSize(@Nullable final String userId);

}
