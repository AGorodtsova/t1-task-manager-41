package ru.t1.gorodtsova.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    SqlSession getSqlSession();

    @NotNull
    EntityManagerFactory getEntityManagerFactory();

}
