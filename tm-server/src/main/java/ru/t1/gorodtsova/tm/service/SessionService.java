package ru.t1.gorodtsova.tm.service;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.repository.ISessionRepository;
import ru.t1.gorodtsova.tm.api.service.IConnectionService;
import ru.t1.gorodtsova.tm.api.service.ISessionService;
import ru.t1.gorodtsova.tm.exception.field.IdEmptyException;
import ru.t1.gorodtsova.tm.dto.model.SessionDTO;

public final class SessionService implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;

    public SessionService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO create(@NotNull final SessionDTO session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.add(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return findOneById(id) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    public SessionDTO findOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        try (SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            @Nullable final SessionDTO session = repository.findOneById(id);
            return session;
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public SessionDTO removeOneById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable SessionDTO session = findOneById(id);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository repository = sqlSession.getMapper(ISessionRepository.class);
            repository.removeOneById(id);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
        return session;
    }

}
