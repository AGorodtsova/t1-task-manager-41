package ru.t1.gorodtsova.tm.dto.request.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.dto.request.AbstractIdRequest;

@NoArgsConstructor
public final class TaskShowByIdRequest extends AbstractIdRequest {

    public TaskShowByIdRequest(@Nullable final String token, @Nullable final String id) {
        super(token, id);
    }

}
