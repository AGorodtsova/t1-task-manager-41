package ru.t1.gorodtsova.tm.exception.user;

public final class IncorrectDataException extends AbstractUserException {

    public IncorrectDataException() {
        super("Error! Incorrect login or password entered. Please try again...");
    }

}
